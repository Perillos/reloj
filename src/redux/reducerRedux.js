
import { applyMiddleware, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"


const INITIAL_STATE = {
    time: 0,
    timerOn: false,
}


const reducer = (state = INITIAL_STATE, action) => {
    switch(action.type) {
        case 'start':
            return {...state, timerOn: true}
        case 'stop':
            return {...state, timerOn: false}
        case 'resume':
            return {...state, timerOn: true}
        case 'reset':
            return {...state, time: 0}
        case 'addTime':
            return {...state, time: state.time + action.payload}
        default:
            return state
    }

}

const store = createStore(reducer, composeWithDevTools(applyMiddleware(thunk)))

export default store;
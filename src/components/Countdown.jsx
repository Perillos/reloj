import { useEffect, useState } from "react";

const Countdown = () => {
    const [time, setTime] = useState('');
    const [myTime, setMyTime] = useState('Jan 1, 2023 12:12:12')

    const handInpud = (eve) => {
        const {value} = eve.target
        setMyTime (value)

    }

    const setCoundDraw = (date) => {
        let countDownDate = new Date(date).getTime();
        let x = setInterval(() => {
            let now = new Date().getTime();
            let distance = countDownDate - now;
            let days = Math.floor(distance / (1000 * 60 * 60 * 24));
            let hours = Math.floor((distance % (1000 * 60 * 60 * 24))/ (1000 * 60 * 60));
            let minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            let seconds = Math.floor((distance % (1000 * 60)) / 1000);
    
            setTime(`${days} d ${hours} h ${minutes} m ${seconds} s`);
    
            if (distance < 0) {
                clearInterval(x);
                setTime('Cuidado con la Bomba, Chita');
            }
    
        }, 1000);
        return x
    }

    useEffect(() => {
        let x = setCoundDraw(myTime)
        return () => {
            clearInterval(x)
        }
    }, [myTime]);

    return (
        <div>
            <h2>Pon una fecha</h2>
            <input type='datetime-local' name='data' onChange={handInpud}/>
            <h2>{time}</h2>
        </div>
    )
}


export default Countdown
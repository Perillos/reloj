import { useEffect, useState } from "react";
import { connect, useDispatch } from "react-redux";


const Crono = ({time, timerOn}) => {
    const dispatch = useDispatch(); // Podemos importar el dispach sin necesidad de conec

    useEffect(() => {
        let interval = null;

        if (timerOn) {
            interval = setInterval(() => {
                dispatch({type: 'addTime', payload: 10})
            }, 10);
        } else {
            clearInterval(interval);
        }
        return () => clearInterval(interval)
    }, [timerOn])


    return (
        <div className='crono'>
            <h2>
                <span>
                    {('0' + Math.floor((time/60000) % 60)).slice(-2)}:
                </span>
                <span>
                    {('0' + Math.floor((time/1000) % 60)).slice(-2)}:
                </span>
                <span>
                    {('0' + Math.floor((time/10) % 100)).slice(-2)}
                </span>
            </h2>
            <div id='buttons'>
                {!timerOn && time === 0 && (
                    <button onClick={() => dispatch({type: 'start'})}>Start</button>
                )}
                {timerOn && 
                    <button onClick={() => dispatch({type: 'stop'})}>Stop</button>
                }
                {!timerOn && time > 0 && (
                    <button onClick={() => dispatch({type: 'resume'})}>Resume</button>
                )}
                {time > 0 && (
                    <button onClick={() => dispatch({type: 'reset'})}>Reset</button>
                )}
            </div>

        </div>
    )
}

// export default Crono

export default connect(state => ({time: state.time, timerOn: state.timerOn }))(Crono)
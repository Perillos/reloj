import DigitalClock from './components/DigitalClock';
import Countdown from './components/Countdown';
import Stopwatch from './components/Stopwatch';
import './App.scss';
import Crono from './components/Crono';

function App() {
  return (
    <div className="app">
      {/* <DigitalClock /> */}
      {/* <Countdown /> */}
      {/* <Stopwatch /> */}
      <Crono />
    </div>
  );
}

export default App;
